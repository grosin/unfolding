import vipunfolding as unfld
import numpy as np

binning=[0,14.8333,19.6667,24.5,29.3333,34.1667,39,43.8333,48.6667,53.5,58.3333,63.1667,68,72.8333,77.6667,82.5,87.3333,92.1667,97,101.833,106.667,111.5,116.333,121.167,126,130.833,135.667,14\
0.5,145.333,150.167,155,159.833,164.667,169.5,174.333,179.167,184,188.833,193.667,198.5,203.333,208.167,213,217.833,222.667,227.5,232.333,237.167,242,246.833,251.667,256.5,261.333,266.167,271\
,275.833,280.667,285.5,290.333,295.167,300]

model=unfld.Model("example",binning,default_histfile="test.root", responsenames={"Response":"JetPt_reco_truth","effCorrDenom":"JetPt", "effCorrNum":"JetPt", "fidCorrDenom":"JetPt", "fidCorrNum":"JetPt"})
model.add_variation("nominal","test.root")
configpath=unfld.unfold(model,"test.root:JetPt", "Bayesian3", mc_as_data=True, xmethod="BinByBin", normalized=False, debug=True)















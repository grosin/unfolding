#include <algorithm>
#include <vector>
#include "TRandom.h"
#include "TH1F.h"
#include "RooUnfoldBayes.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "TVectorD.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldSvd.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "Unfold.h"
#include "TCut.h"
#include <cstring>
#include "TMath.h"
#include <memory>
#include <fstream>
#include <TTree.h>
#include "TH2F.h"
#include "TAxis.h"
#include <TRandom.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace TMath;

bool GevCorrection=false;
TRandom *RNG= new TRandom(0);



void fill_map(){
  distribution_maps["detajj"].binning=binning_jet_deta;
  distribution_maps["detajj"].bins=jet_deta_bins;
  distribution_maps["dphijj"]={binning_jet_dphi,jet_dphi_bins};
  distribution_maps["mjj"]={binning_mjj,mjj_bins};
  distribution_maps["drjj"]={binning_jet_dr,jet_dr_bins};
  distribution_maps["dphill"]={binning_lep_dphi,lep_dphi_bins};
  distribution_maps["drll"]={binning_lep_dr,lep_dr_bins};
  distribution_maps["detall"]={binning_lep_deta,lep_deta_bins};
  distribution_maps["met"]={binning_met,met_bins};
  distribution_maps["mll"]={binning_mll,mll_bins};


}
void itoa(float number, char* str) {
   sprintf(str, "%.1f", number); 
}
int UnfoldDist(int iterations,const char * dist,float sigma, bool gevCorr,bool vertx, bool lead,bool draw,int gaussian_iters){
  fill_map();
  GevCorrection=gevCorr;
  TFile* TruthVBFFile = TFile::Open("ntuples/merged-output.root");
  TNtuple *truth_VBF = (TNtuple*)TruthVBFFile->Get("tree");

  const char * truth_distribution=dist;
  char result[1024];
  result[0]='\0';
  strcat(result, "r_");
  strcat(result,dist);
  const char * reco_distribution=result;
  
  long entries=truth_VBF->GetEntries();
  //std::vector <double> minMax=GetMinMax(truth_VBF,truth_distribution);
  TH1F *truth_test=new TH1F("test_truth",truth_distribution,distribution_maps[dist].bins,distribution_maps[dist].binning);
  TH1F *reco_test=new TH1F("test_reco",reco_distribution,distribution_maps[dist].bins,distribution_maps[dist].binning);
  TH2F * migration_matrix=new TH2F("migrations_matrix","migrations_matrix",distribution_maps[dist].bins,distribution_maps[dist].binning,distribution_maps[dist].bins,distribution_maps[dist].binning);
  std::cout<<"filling response with truth info\n";
  RooUnfoldResponse response;
  if(StringInVec(dist,scalars)){
    response=create_response(truth_VBF, truth_distribution,reco_distribution,truth_test,reco_test,migration_matrix);
  }
  if(StringInVec(dist,jets)){
    response=jet_lep_response(truth_VBF, truth_distribution,truth_test,reco_test,lead,migration_matrix,true);
  }
    if(StringInVec(dist,leps)){
      response=jet_lep_response(truth_VBF, truth_distribution,truth_test,reco_test,lead,migration_matrix,false);
    }
  if(iterations==1)
    print_matrix(response.Mresponse(),dist);

  double chi2;
  double residue;
  RooUnfoldBayes unfolded;
  
  unfolded=unfold_bayes(response,reco_test,iterations);
  
  print_bins((TH1F*)unfolded.Hreco(),dist,iterations);
  
  unfolding_uncertanty((TH1F*)unfolded.Hreco(),dist,iterations);
  //throw_toys(response, truth_test, reco_test,sigma,1000,iterations);
  
  if(draw)
    print_and_draw(unfolded,truth_test,reco_test);
  

  ofstream myfile;
  myfile.open("unfolding_data.txt",ios::app);
  myfile<<dist<<" ";
  myfile<<iterations<<" ";
  myfile<<chi2<<" ";
  myfile<<residue<<"\n";
  myfile.close();
  
  //TruthVBFFile->Close();
  // delete truth_VBF;

  //delete truth_test;
  //delete reco_test;
  //delete TruthVBFFile;
  
  
  return chi2;

}
void throw_toys(RooUnfoldResponse &response, TH1F* truth,TH1F* reco,int sigma,int toys,int iterations){

 for(int i =0 ;i <toys;i++){
    TH1F* truth_test_weighted=(TH1F*)truth->Clone("truth_test_weighted");
    TH1F *reco_test_weighted=(TH1F*)reco->Clone("reco_test_weighted");
    
    RooUnfoldBayes unfolded=unfold_bayes(response,reco_test_weighted,iterations);
    //save iteration to datafile 
    int chi2=calculate_chi2((TH1F*)unfolded.Hreco(),truth_test_weighted,sigma,truth_test_weighted->GetName(),iterations);
  }
}

void reweight_hists(TH1F *truth, TH1F* reco,float sigma, TH2F * migration){
    int nbins=truth->GetSize();
    double bin_content=0;
    double bin_error=0;
    double fluctuation=0;
    double reco_bin_content=0;
    //reweight reco
    for(int i=0;i<nbins;i++){
      bin_content=truth->GetBinContent(i);
      reco_bin_content=reco->GetBinContent(i);
      bin_error=reco->GetBinError(i);
      fluctuation=RNG->Gaus(0,sigma*bin_error);
      //std::cout<<"fluctuation: "<<fluctuation<<"\n";
      reco->SetBinContent(i,bin_content+fluctuation);
      //reco->SetBinContent(i,reco_bin_content+fluctuation*(reco_bin_content/bin_content));
 }
    //fold into truth

    for(int i=0;i<nbins;i++){
      double reco_migration_sum=0;
      double truth_migration_sum=0;
      for(int j=0;j<nbins;j++){
        reco_migration_sum=reco_migration_sum+migration->GetBinContent(i,j);
      }
      
      bin_content=reco->GetBinContent(i)*truth->GetBinContent(i)/reco_migration_sum;
      truth->SetBinContent(i,bin_content) ;
    }

}

void reroll_hists(TH1F *truth, TH1F* reco,float sigma){
    int nbins=truth->GetSize();
    double bin_content=0;
    double bin_error=0;
    double fluctuation=0;
    double reco_bin_content=0;
    //reweight reco
    for(int i=0;i<nbins;i++){
      bin_content=truth->GetBinContent(i);
      reco_bin_content=reco->GetBinContent(i);
      bin_error=truth->GetBinError(i);
      fluctuation=RNG->Gaus(0,sigma*bin_error);
      //std::cout<<"fluctuation: "<<fluctuation<<"\n";
      truth->SetBinContent(i,bin_content+fluctuation);
      reco->SetBinContent(i,reco_bin_content+fluctuation*(reco_bin_content/bin_content));
      std::cout<<"fluctuation: "<<fluctuation<<"\n";
    }
    

}


                    
void effeciency(TH1F * reco_test, TH1F *truth_test, const char *dist){
  reco_test->Divide(truth_test);
  char title[1024];
  title[0]='\0';
  strcat(title, "Reconstruction Effeciency for ");
  strcat(title,dist);
  reco_test->SetTitle(title);
  reco_test->Draw();

}


RooUnfoldResponse create_response(TTree *data_tree, const char * truth_distribution,const char * detector_distribution,TH1F * truth_hist,TH1F * reco_hist,TH2F *migration){
  std::cout<<"creatning response matrix\n";
  float truth_data;
  float detector_data;
  float weight;
  int passReco;
  int fiducial;
  int preSelection;
  int passedCJV;
  int passedOLV;
  
  data_tree->SetBranchAddress(truth_distribution, &truth_data);
  data_tree->SetBranchAddress(detector_distribution, &detector_data);
  data_tree->SetBranchAddress("weight", &weight);
  data_tree->SetBranchAddress("r_passedRecoSave",&passReco);
  data_tree->SetBranchAddress("r_passedFiducialSave",&fiducial);
  data_tree->SetBranchAddress("r_passedPreselection",&preSelection);
  data_tree->SetBranchAddress("r_passedCJV",&passedCJV);
  data_tree->SetBranchAddress("r_passedOLV",&passedOLV);
  
  Int_t truth_nevents = data_tree->GetEntries();
  
 
  bool truth_cut;
  bool reco_cut;
  std::cout<<"bin_range:"<<min_bin<<" "<<max_bin<<"\n";
  RooUnfoldResponse response (reco_hist,truth_hist);

  float corr;
  GevCorrection ? corr=1000 : corr=1;
  
  for (Int_t i=0;i<truth_nevents;i++){
    data_tree->GetEvent(i);
    truth_cut=(fiducial==1 && preSelection==1 && passedCJV==1 && passedOLV==1);
    reco_cut= passReco==1 ;
    if(truth_cut && reco_cut){
      response.Fill(detector_data/corr,truth_data,weight);
      migration->Fill(detector_data/corr,truth_data,weight);
      truth_hist->Fill(truth_data,weight);
      reco_hist->Fill(detector_data/corr,weight);
    }
    else if(truth_cut){
      response.Miss(truth_data,weight);
      truth_hist->Fill(truth_data,weight);
    }
  }
 
  return response;
}


RooUnfoldResponse jet_lep_response(TTree *truth_VBF, const char * distribution,TH1F * truth_hist,TH1F * reco_hist,bool leading,TH2F * migration,bool is_jet){
  std::string var;
  is_jet ?  var="jet" :var="lep";
  std::cout<<"creatning response matrix for vertex\n";
  std::cout<<"truth dist "<<distribution<<"\n";

  std::cout<<"bin_range:"<<min_bin<<" "<<max_bin<<"\n";
  RooUnfoldResponse response (reco_hist,truth_hist);

  TH2F * jet_phi_corr= new TH2F((var+"_phi_corr").c_str(),(var+" phi correlation").c_str(),50,min_bin,max_bin,50,min_bin,max_bin);
  std::vector<float>* truth_phi=new std::vector<float>();
  std::vector<float> *detector_phi= new std::vector<float>();

  std::vector<float>* truth_eta=new std::vector<float>();
  std::vector<float> *detector_eta= new std::vector<float>();
    
  float weight;
  int passReco;
  int fiducial;
  int preSelection;
  int passedCJV;
  int passedOLV;
  std::cout<<(var+"_phi").c_str()<<"\n";
  std::cout<<("r_"+var+"_phi").c_str()<<"\n";
  truth_VBF->SetBranchAddress((var+"_phi").c_str(), &truth_phi);
  truth_VBF->SetBranchAddress(("r_"+var+"_phi").c_str(), &detector_phi);

  truth_VBF->SetBranchAddress((var+"_eta").c_str(), &truth_eta);
  truth_VBF->SetBranchAddress(("r_"+var+"_eta").c_str(), &detector_eta);

  truth_VBF->SetBranchAddress("weight", &weight);
  truth_VBF->SetBranchAddress("r_passedRecoSave",&passReco);
  truth_VBF->SetBranchAddress("r_passedFiducialSave",&fiducial);
  truth_VBF->SetBranchAddress("r_passedPreselection",&preSelection);
  truth_VBF->SetBranchAddress("r_passedCJV",&passedCJV);
  truth_VBF->SetBranchAddress("r_passedOLV",&passedOLV);
  
  Int_t truth_nevents = truth_VBF->GetEntries();
  
 
  bool truth_cut;
  bool reco_cut;
  std::cout<<"bin_range:"<<min_bin<<" "<<max_bin<<"\n";
  float corr;
  int index;
  GevCorrection ? corr=1000 : corr=1;
  double dphi;
  double r_dphi;
  double deta;
  double r_deta;
  double dr;
  double r_dr;
  std::cout<<"starting loop \n";
  std::cout<<truth_nevents<<"\n";
  //migration->Fill(2,2);

  for (int j=0;j<truth_nevents;j++){
    try{
      truth_VBF->GetEvent(j);
      truth_cut=(fiducial==1 && preSelection==1 && passedCJV==1 && passedOLV==1);
      reco_cut= passReco==1 ;
      
      if(truth_cut && reco_cut){
        
        dphi=truth_phi->at(0)-truth_phi->at(1);
        r_dphi=detector_phi->at(0) - detector_phi->at(1);
        deta=truth_eta->at(0)-truth_eta->at(1);
        r_deta=detector_eta->at(0) - detector_eta->at(1);        
        dr=Power(Power(deta,2)+Power(dphi,2),0.5);
        r_dr=Power(Power(r_deta,2)+Power(r_dphi,2),0.5);
        
        if (check_string("deta",distribution)){
          truth_hist->Fill(deta,weight);
          reco_hist->Fill(r_deta,weight);
          migration->Fill(2.5,2.5);
          response.Fill(r_deta,deta,weight);
        }
        else if (check_string("dphi",distribution)){
          truth_hist->Fill(dphi,weight);
          reco_hist->Fill(r_dphi,weight);
          migration->Fill(r_dphi,deta,weight);
          response.Fill(r_dphi,dphi,weight);
        }
        else if (check_string("dr",distribution)){
          truth_hist->Fill(dr,weight);
          reco_hist->Fill(r_dr,weight);
          migration->Fill(r_dr,dr,weight);
          response.Fill(r_dr,dr,weight);
        }

      
      }
      else if(truth_cut){
        try{
          dphi=truth_phi->at(0)-truth_phi->at(1);
          deta=truth_eta->at(0)-truth_eta->at(1);
          dr=Power(Power(deta,2)+Power(dphi,2),0.5);
          if (check_string("deta",distribution)){
            response.Miss(deta,weight);
            truth_hist->Fill(deta,weight);
          }
          else if (check_string("dphi",distribution)){
            response.Miss(dphi,weight);
            truth_hist->Fill(dphi,weight);
          }
          else if (check_string("dr",distribution)){
            response.Miss(dr,weight);
            truth_hist->Fill(dr,weight);
          }
           
        }catch(std::out_of_range &e){
          std::cout<<"out of range issues in truth\n";
        }
      }
    }catch(std::out_of_range &e){
          std::cout<<"out of range issues in general\n";
    }
  }


  TCanvas *c1 = new TCanvas("c1", "c1",1);
 
  return response;


}
RooUnfoldBayes unfold_bayes(RooUnfoldResponse &response, TH1F* reco,int iterations){
  RooUnfoldBayes unfold (&response, reco, iterations);
  return unfold;
}

RooUnfoldSvd unfold_svd(RooUnfoldResponse &response, TH1F* data,int normalization){
  RooUnfoldSvd unfold (&response, data, normalization);
  return unfold;
}

void print_and_draw(RooUnfoldBayes &unfold, TH1F * Test_Truth_Hist,TH1F* Reco_Test ){
  TH1F* hReco= (TH1F*) unfold.Hreco();
  unfold.PrintTable (cout,Test_Truth_Hist);

  TCanvas *c1 = new TCanvas("c1", "c1",1000,1000);
  TPad *pad1 = new TPad("pad1", "pad1",0.,.3,1.,1);
  pad1->SetBottomMargin(0);
  pad1->Draw();
  pad1->cd();
  hReco->SetMarkerColor(kBlack);
  hReco->SetMarkerStyle(kFullCircle);
  hReco->SetMarkerSize(0.8);
  Reco_Test->SetLineColor(kGreen);
  
  auto legend = new TLegend(0.1,0.8,0.48,0.9);
  legend->AddEntry(Test_Truth_Hist,"Truth  distribution","f");
  legend->AddEntry(hReco,"Unfolded distribution","lep");
  legend->AddEntry(Reco_Test,"Reco distribution","f");
  Test_Truth_Hist->SetMinimum(0);
  Test_Truth_Hist->Draw();
  hReco->Draw("SAME");
  Reco_Test->Draw("SAME");
  legend->Draw();
  Test_Truth_Hist->GetYaxis();
  TGaxis *axis = new TGaxis( -5, 20, -5, 220, 20,220,510,"");
  axis->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  axis->SetLabelSize(15);
  axis->Draw();
  c1->cd();
  TPad *pad2 = new TPad("pad2", "pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.2);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();

  TH1F *h3 = (TH1F*)Test_Truth_Hist->Clone("h3");
  h3->Divide(hReco);
  h3->Draw();

  Test_Truth_Hist->GetYaxis()->SetTitleSize(20);
  Test_Truth_Hist->GetYaxis()->SetTitleFont(43);
  Test_Truth_Hist->GetYaxis()->SetTitleOffset(1.55);
  h3->SetMaximum(1.5);
  h3->SetMinimum(0.5);
  h3->GetYaxis()->SetTitle("ratio unfold/truth");
  h3->SetStats(0);
  h3->GetYaxis()->SetNdivisions(505);
  h3->GetYaxis()->SetTitleSize(20);
  h3->GetYaxis()->SetTitleFont(43);
  h3->GetYaxis()->SetTitleOffset(1.55);
  h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetYaxis()->SetLabelSize(15);

   // X axis ratio plot settings
  h3->GetXaxis()->SetTitleSize(20);
  h3->GetXaxis()->SetTitleFont(43);
  h3->GetXaxis()->SetTitleOffset(4.);
  h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetXaxis()->SetLabelSize(15)  ;                             
}

void print_and_draw(RooUnfoldSvd &unfold, TH1F * Test_Truth_Hist,TH1F* Reco_Test ){
  TH1F* hReco= (TH1F*) unfold.Hreco();
  unfold.PrintTable (cout,Test_Truth_Hist);
  hReco->SetMarkerColor(kBlack);
  hReco->SetMarkerStyle(kFullCircle);
  hReco->SetMarkerSize(0.8);
  Reco_Test->SetLineColor(kGreen);
  TCanvas *c1 = new TCanvas("c1", "c1",1);
  auto legend = new TLegend(0.1,0.8,0.48,0.9);
  legend->AddEntry(Test_Truth_Hist,"Truth  distribution","f");
  legend->AddEntry(hReco,"Unfolded distribution","lep");
  legend->AddEntry(Reco_Test,"Reco distribution","f");
  
  Test_Truth_Hist->Draw();
  hReco->Draw("SAME");
  Reco_Test->Draw("SAME");
  legend->Draw();
}


void print_matrix(const TMatrixD &response,const char * distribution){
  ofstream matrix_file ;
  matrix_file.open("response_matrix.txt",ios::app);
  matrix_file<<"variable:"<<distribution<<"\n";
  
  int nrows=response.GetNrows();
  int ncols=response.GetNcols();
  matrix_file<<"size:"<<nrows<<","<<ncols<<"\n";

  for(int i=0;i<nrows;i++){
    for(int j=0;j<ncols;j++){
      std::cout<<response[i][j]<<" ";
      matrix_file<<response[i][j]<<" ";
    }
    std::cout<<"\n";
    matrix_file<<"\n";
  }
  matrix_file.close();
}


//get minimum and maximum value of a variable
vector<double> GetMinMax(TTree * VBF,const char * distribution){
  gROOT->SetBatch(true);
  long nevent = VBF->GetEntries();
  VBF->SetEstimate(nevent);
  VBF->Draw(distribution,"r_passedFiducialSave==1");//,distribution_cut);
  double * tree_data=VBF->GetV1();
  std::cout<<"calculating MinMax: "<<nevent<<"\n";
  double* data_min_bin=std::min_element(tree_data, tree_data+nevent);
  double* data_max_bin=std::max_element(tree_data,tree_data+nevent);
  std::cout<<"min :"<<*data_min_bin<<"max: "<<*data_max_bin<<"\n";
  gROOT->SetBatch(false);
  std::vector<double> MinMax(2);
  MinMax[0]=*data_min_bin;
  MinMax[1]=*data_max_bin;
  if(*data_max_bin >100)
    GevCorrection=true;
  else
    GevCorrection=false;
                       
  return MinMax; 
}




void loop_vectors(){
  
  TH1F::AddDirectory(kFALSE);
  TFile* TruthVBFFile = TFile::Open("merged-output.root");
  TNtuple *truth_VBF = (TNtuple*)TruthVBFFile->Get("tree");

  const char * truth_distribution="jet_eta";
  const char * reco_distribution="r_jet_eta";
  std::vector <double> minMax=GetMinMax(truth_VBF,truth_distribution);
  
  min_bin=-7;
  max_bin=7;
  RooUnfoldResponse response (nbins,min_bin ,max_bin);

  std::cout<<"Maximum BIN "<<max_bin<<"Minimum BIN "<<min_bin<<"\n";
  TH1F *truth_hist=new TH1F("test_truth",truth_distribution,nbins,min_bin,max_bin);
  TH1F *reco_hist=new TH1F("test_reco",reco_distribution,nbins,min_bin,max_bin);

  std::cout<<"creatning response matrix for vertex\n";
  std::vector<float>* truth_data=new std::vector<float>();
  std::vector<float> *detector_data= new std::vector<float>();
  float weight;
  int passReco;
  int fiducial;
  int preSelection;
  int passedCJV;
  int passedOLV;
  
  truth_VBF->SetBranchAddress(truth_distribution, &truth_data);
  truth_VBF->SetBranchAddress(reco_distribution, &detector_data);
  truth_VBF->SetBranchAddress("weight", &weight);
  truth_VBF->SetBranchAddress("r_passedRecoSave",&passReco);
  truth_VBF->SetBranchAddress("r_passedFiducialSave",&fiducial);
  truth_VBF->SetBranchAddress("r_passedPreselection",&preSelection);
  truth_VBF->SetBranchAddress("r_passedCJV",&passedCJV);
  truth_VBF->SetBranchAddress("r_passedOLV",&passedOLV);
  
  Int_t truth_nevents = truth_VBF->GetEntries();
  
 
  bool truth_cut;
  bool reco_cut;
  std::cout<<"bin_range:"<<min_bin<<" "<<max_bin<<"\n";

  float corr;
  int index;
  GevCorrection ? corr=1000 : corr=1;
  
  for (Int_t i=0;i<truth_nevents;i++){
    truth_VBF->GetEvent(i);
    truth_cut=(fiducial==1);
    reco_cut= passReco==1 ;
    if(truth_cut && reco_cut){
      truth_hist->Fill(truth_data->at(0)-truth_data->at(1));
      reco_hist->Fill(detector_data->at(0)-detector_data->at(1));
      response.Fill(truth_data->at(0)-truth_data->at(1),detector_data->at(0)-detector_data->at(1));

    }
    else if(truth_cut){
      truth_hist->Fill(truth_data->at(0)-truth_data->at(1));
      response.Miss(truth_data->at(0)-truth_data->at(1));
    }
  }

   TruthVBFFile->Close();
   std::cout<<"drawing truth hist\n";
   // TCanvas *c2 = new TCanvas("c1", "c1",1000,1000);

   //truth_hist->Draw();
   RooUnfoldBayes unfolded=unfold_bayes(response,reco_hist,2);
     
   print_and_draw(unfolded,truth_hist,reco_hist);
}


double calculate_residuals(TH1F * unfolded, TH1F *truth){
  double residual=0;
  for (Int_t i=0;i<unfolded->GetNbinsX();i++){
    residual+=Abs(unfolded->GetBinContent(i)-truth->GetBinContent(i));
  }
  return residual;
}
               
double calculate_chi2(TH1F * unfolded, TH1F *truth, float sigma, const char* dist,int iters){
  double chi2=0;
  ofstream chiFile ;
  char filename[1024];
  char sigma_buffer[10];
  itoa(sigma,sigma_buffer);
  filename[0]='\0';
  strcat(filename,dist);
  strcat(filename, "_sig_");
  strcat(filename,sigma_buffer);

  strcat(filename, "statistical_data");    
  strcat(filename,".txt");

  chiFile.open(filename,ios::app);
  chiFile << dist<<",";
  chiFile << iters;
  chiFile<<",";
  double bin_residue(0);
  for (Int_t i=0;i<unfolded->GetNbinsX();i++){
    bin_residue=unfolded->GetBinContent(i)-truth->GetBinContent(i);
    chiFile<<bin_residue;
    chiFile<<",";
    if(unfolded->GetBinError(i)>0)
      chi2+=Power(unfolded->GetBinContent(i)-truth->GetBinContent(i),2)/Power(unfolded->GetBinError(i),2);
  }
  chiFile<<chi2;
  chiFile<<"\n";
  chiFile.close();
  return chi2;
}
void unfolding_uncertanty(TH1F * unfolded,const char *dist, int iters){
  ofstream chiFile ;
  chiFile.open("unfolding_uncertinty.txt",ios::app);
  chiFile<<dist<<","<<iters<<",";

  for (Int_t i=0;i<unfolded->GetNbinsX();i++){
    chiFile<<unfolded->GetBinError(i)<<",";
  }
  chiFile<<"\n";
  chiFile.close();
}
void print_bins(TH1F * hist,const char* dist,int iters){
  
  ofstream chiFile ;
  chiFile.open("binning_error.txt",ios::app);
  chiFile<<dist<<","<<iters<<",";
  for (Int_t i=0;i<hist->GetNbinsX();i++){
    chiFile<<hist->GetBinContent(i)<<",";
  }
  chiFile<<"\n";
  chiFile<<"uncertainty,";
  for (Int_t i=0;i<hist->GetNbinsX();i++){
    chiFile<<hist->GetBinError(i)<<",";
  }
  chiFile<<"\n";
  chiFile.close();
}

//drjj
//drjj cuts
//delta phi and eta comparison
//muon vs. electron cut
//change error bars to  come from unfolding uncertanty
//Mt
//input vairables



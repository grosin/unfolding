#ifndef UNFOLD
#define UNFOLD

#include <algorithm>
#include <vector>
#include "TRandom.h"
#include "TH1F.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldSvd.h"

#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "TVectorD.h"
#include "RooUnfoldResponse.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TMatrixD.h"
#include "TCut.h"
#include "TTree.h"
#include <map>
#include <string>

TRandom *dice=new TRandom(1);
const int nbins=20;
int min_bin=0;
int max_bin=0;

//prototypes
TCut distribution_cut;

/****** main functions *************/
int UnfoldDist(int iterations,const char * dist,float sigma=5.0,bool gevCorr=false,bool vertx=false, bool lead=true,bool Draw=true,int gaussian_iters=100);


/****** create response matrices *************/
RooUnfoldResponse create_response(TTree *data_tree, const char * truth_distribution,const char * detector_distribution,TH1F * truth_hist,TH1F * reco_hist,TH2F *migration);
RooUnfoldResponse jet_lep_response(TTree *truth_VBF, const char * distribution,TH1F * truth_hist,TH1F * reco_hist,bool leading,TH2F * migration,bool is_jet);

/****** unfold *************/
RooUnfoldBayes unfold_bayes(RooUnfoldResponse &response, TH1F* data,int iterations);
RooUnfoldSvd unfold_svd(RooUnfoldResponse &response, TH1F* data,int iterations);

/****** draw *************/
void print_and_draw(RooUnfoldBayes &unfold, TH1F * TestHist, TH1F *);
void print_and_draw(RooUnfoldSvd &unfold, TH1F * TestHist, TH1F *);


/********* utility functions *****************/
void print_matrix(const TMatrixD &response, const char *);
vector<double> GetMinMax(TTree * data_tree,const char * distribution);
double calculate_residuals(TH1F * unfolded, TH1F *truth);
double calculate_chi2(TH1F * , TH1F *,float sigma, const char* dist="undefined",int iterations=1);
void reroll_hists(TH1F *truth, TH1F* reco,float sigma,TH2F*);
void reweight_hists(TH1F *truth, TH1F* reco,float sigma);
void throw_toys(RooUnfoldResponse &response, TH1F* truth,TH1F* reco,int sigma,int toys,int);

void print_bins(TH1F * hist,const char* dist,int iters);
void itoa(float number,char*);
void unfolding_uncertanty(TH1F * unfolded,const char *dist, int iters);

bool StringInVec(std::string s,std::vector<std::string> &vec){
  return std::find(vec.begin(), vec.end(),s) != vec.end();
}

bool check_string(std::string substring,std::string full){
  return full.find(substring) != std::string::npos;
}

/******************define binning ******************/

struct distribution_container 
{  
  double* binning;
  int bins;
};

std::vector<std::string> scalars ={"mjj","mll","met"};
std::vector<std::string> leps={"detall","dphill","drll","lead_lep_pt"};
std::vector<std::string> jets={"detajj","dphijj","drjj","led_jet_pt"};

std::map<std::string,distribution_container> distribution_maps;

double binning_jet_deta[]={-6.2,-4,-3,-2,0,2,3,4,6.2};int jet_deta_bins(8);

double binning_jet_dphi[]{-6.2,-4,-3,-2,-1,0,1,2,3,4,6.2};int jet_dphi_bins(10);
double binning_jet_dr[]={0,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,9,10};int jet_dr_bins(15);
double binning_lead_jet_eta[]={-5,-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3,3.5,4,5};int lead_jet_eta_bins(17);
//distribution_maps["lead_jet_eta"]={binning_lead_jet_eta,lead_jet_eta_bins};
double binning_lead_jet_phi[]={-3.2,-2.5,-2,-1.5,-1,-0.5,0,0.5,1,1.5,2,2.5,3.2};int lead_jet_phi_bins(12);
//distribution_maps["lead_jet_phi"]={binning_lead_jet_phi,lead_jet_phi_bins};
double binning_lead_jet_pt[]={0,25,50,75,100,125,150,175,200,225,250,275,300};int jet_pt_bins(12);
//distribution_maps["lead_jet_pt"]={binning_lead_jet_pt,lead_jet_pt_bins};
  
double binning_lep_deta[]={-2.5,-2,-1.5,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.5,2,2.5};int lep_deta_bins(14);
double binning_lep_dphi[]={-6.2,-5,-4,-3,-2,-1,-0.4,0.2,0.8,1.4,2,3,4,5,6.2};int lep_dphi_bins(14);
double binning_lep_dr[]={0,0.2,0.6,1.2,1.6,2,2.5,3.5,4,4.5,5,5.25,5.5,5.75,6,6.5};int lep_dr_bins(15);
double binning_met[]={0,25,50,75,100,125,150,200,300};int met_bins(8);
double binning_mjj[]={0,250,500,750,1000,1250,1500,1750,2000,2250,2500,3000,4000,5000};int mjj_bins(13);
double binning_mll[]={0,7.5,15,20,25,30,35,40,45,50,55,60,70,80,100};int mll_bins(14);
#endif


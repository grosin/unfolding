#include<iostream>
#include <TH1F.h>
#include <TRandom.h>
#include <TFile.h>
#include <TH1F.h>
#include <string>
void variation(int radint){
//load reco histogram and response matrix                                                                                                                                                       

  TFile * hist_file=TFile::Open("vip_hists.root","UPDATE");
  TH1F * truth_hist=(TH1F*)hist_file->Get("no_cuts_detall_true");
  TH1F * reco_hist=(TH1F*)hist_file->Get("no_cuts_detall_reco");

  TH2F * migration_matrix=(TH2F*)hist_file->Get("no_cuts_detall_migration");
  TRandom *RNG= new TRandom();
  RNG->SetSeed(0);
  //only thign to do in variation is reweigh purity denom as eff_num * truth weight/reco weight                                                                                                 
  int nbins=truth_hist->GetSize();

  double bin_content=0;
  double bin_error=0;
  double fluctuation=0;
  double reco_bin_content=0;
  TH1F  * truth_variation=(TH1F*)truth_hist->Clone();
  truth_variation->SetName(std::string("detall_true_variation"+std::to_string(radint)).c_str());
  TH1F * reco_variation=(TH1F*)truth_variation->Clone();  
  reco_variation->SetName(std::string("detall_reco_variation"+std::to_string(radint)).c_str());
  for(int i=0;i<nbins;i++){
    bin_content=truth_hist->GetBinContent(i);
    reco_bin_content=reco_hist->GetBinContent(i);
    bin_error=truth_hist->GetBinError(i);
    fluctuation=RNG->Gaus(0,10*bin_error);
    std::cout<<fluctuation<<"\n";
    truth_variation->SetBinContent(i,bin_content+fluctuation);
    reco_variation->SetBinContent(i,reco_bin_content+fluctuation);
 }
 

  std::cout<<"writing to file\n";
  //hist_file->mkdir("truth_variations");
  //hist_file->mkdir("reco_variations");
  hist_file->cd("truth_variations");
  truth_variation->SetDirectory(gDirectory);
  truth_variation->Write();
  hist_file->cd("reco_variations");
  reco_variation->SetDirectory(gDirectory);
  reco_variation->Write();
  //hist_file->Close();


}

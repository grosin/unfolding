import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit

data_folder="fluctuation_data_files/"
MJJ_BINNING=[0.01,58957.4,165296,142519,103852,72963.7,51284.9,37008.8,26838.7,19177.9,14067.4,17364.8,13942.2]
MJJ_UNCERT=[0,1187.1,1852.18,1574.36,1247.39,969.063,763.324,621.936,502.249,409.53,341.632,493.618,493.005]

MLL_BINNING=[0.01,0.01,73398.7,48150.4,84361,99615.1,112596,79897.1,55292.1,68578.3,33539.1,37322.7,46355.4,8168.92]
MLL_UNCER=[0.01,0.01,1850.22,1287.68,1341.19,1376.61,1408.39,1383.3,1355.89,1329.47,1194.87,1125.8,2223.61,1537.87]

DETALL_BINNING=[0.01,1031.23,7004.95,33873.7,41429.9,65924.9,99869.9,114930,114731,99146.2,66329.9,41279.9,33491.2,7132.45]
DETALL_UNCERT=[0.01,229.931,551.974,1070.07,1100.69,1292.72,1567.48,1798.97,1809.65,1550.4,1291.29,1089.83,1011.32,531.725]
DPHILL_BINNING=[0.01,14051.7,30498,9433.66,48927.9,86455.8,135836,149188,157664,67033.8,12834.7,26985.9,22252.3,12226,]
DPHILL_UNCERT=[0.01,731.108,736.902,709.943,898.294,1395.54,1599.7,2051.43,1749,1305.05,962.21,944.743,731.699,730.915,]

DRLL_BINNING=[0.01,1947.12,447421,175494,85963.7,13520.3,87.4443,40903.7,28013.4,56447.6,5792.87,32274.2,0.01,0.01,746.83]
DRLL_UNCERT=[0.01,2798.72,1983.14,2380.77,1617.42,1556.14,3.2794,1238.76,730.746,753.632,785.583,553.578,0.01,0.01,466.535]

DETALL_BINNING=[0.01,1031.23,7004.95,33873.7,41429.9,65924.9,99869.9,114930,114731,99146.2,66329.9,41279.9,33491.2,7132.45]
DETALL_UNCERT=[0,230.589,552.25,1070.39,1100.91,1293.04,1568.01,1800.04,1810.71,1550.89,1291.59,1090.04,1011.6,531.914]
#data goes as [iteration][columns][0 for average 1 for std]

DRLL_BINNING=[0.01,16792.4,188535,248801,85387.4,44729.9,28293.7,25447.4,10961.2,13226.2,17264.9,10192.4,11583.7,11718.7,9918.7]
DRLL_UNCERT=[0.01,2798.74,1983.14,2380.77,1617.42,1262.13,1108.07,1238.76,730.746,753.632,785.582,553.578,549.738,506.171,466.538]

MLL_BINNING=[0.01,58957.4,165296,142519,103852,72963.8,51285.1,37008.8,26836.9,19176.6,14075.5,17374.8,13898.9]
MLL_UNCERT=[0.01,1521.41,2391.51,2351.69,2116.44,1867.71,1580.73,1314.35,1039.13,835.416,750.092,986.049,851.416]

DRJJ_BINNING=[0.01,5441.2,9787.45,21483.7,39048.7,60397.4,79019.9,91788.6,95748.7,91091.1,77182.3,61897.4,42584.9,27351.1,21663.3]
DRJJ_UNCERT=[0.01,456.141,536.141,803.186,1054.62,1282.05,1481.13,1602.81,1633.93,1608.7,1492.65,1348.21,1179.75,964.33,924.159]

DETAJJ_BINNING=[0.01,183913,75527.9,37553.6,11695.4,11286.7,38668.5,75172.5]
DETAJJ_UNCERT=[0.01,2361.24,1506.12,1092.9,683.839,707.724,1115.35,1509.09]
def gaussian(x, a, mean, sigma):
    return a * np.exp(-((x - mean)**2 / (2 * sigma**2)))

def load_unfolding_uncertainty():
    ufile=data_folder+"unfolding_uncertinty.txt"
    uncert_dict={}
    with open(ufile,"r") as fl:
        for line in fl:
            line=line[:-1]
            line_data=line.split(",")
            dist=line_data[0]
            iteration=line_data[1]
            bins_data=np.array([float(f) for f in line_data[2:] if f],dtype=np.float64)
            if dist in uncert_dict:
                if iteration in uncert_dict[dist]:
                    uncert_dict[dist][iteration]=np.mean((uncert_dict[dist][iteration],bins_data),axis=0)
                else:
                    uncert_dict[dist][iteration]=bins_data
            else:
                uncert_dict[dist]={iteration:bins_data}
    return uncert_dict
def load_matrices(matrix_file):
    matrices={}
    with open(data_folder+matrix_file) as fl:
        distribution=""
        for line in fl:
            if line.split(":")[0]=="variable":
                distribution=line.split(":")[1].rstrip()
            elif line.split(":")[0]=="size":
                matrices[distribution]=[]
                continue
            else:
                try:
                    line_data=[float(i) for i in line.split()]
                    if line_data:
                        matrices[distribution].append(line_data)
                except ValueError:
                    matrices[distribution]=np.array(matrices[distribution])
                    continue
    return matrices
                
def data_by_bins(data,iterations,columns):
    bin_arrays=[]
    for it in range(1,iterations+1):
        bin_data=[]
        for name in columns:
            iter_data=data.loc[data["iters"]==it,name]
            bin_means=(abs(np.mean(iter_data[1:])),np.std(iter_data))
            bin_data.append(bin_means)
        bin_arrays.append(bin_data)
    return bin_arrays

def load(data_file,columns):
    names=["Dist","iters"]+columns
    return pd.read_csv(data_folder+data_file,names=names)

class BinData:
    def __init__(self,data_file,distribution,num_of_columns,binning,response_matrix,sigma=5,iterations=4):
        self.columns=[f"Column_{i}" for i in range(1,num_of_columns)]
        self.num_of_columns=num_of_columns
        self.raw_data=load(data_file,self.columns).fillna(0).replace([np.inf, -np.inf],0)
        self.processed_data=data_by_bins(self.raw_data,iterations,[f"Column_{i}" for i in range(1,num_of_columns)])
        self.distribution=distribution
        self.sigma=sigma
        self.iterations=iterations
        self.binning=binning
        self.response_matrix=np.array(response_matrix)
        print(self.raw_data.head(10))
        
    def bin_by_bin_histogram(self,iteration):
        i=0
        matrix_data=[]
        for column in self.columns:
            try:
                data=self.raw_data.loc[self.raw_data["iters"]==iteration,column]#/self.binning[i]
                print(data)
                yvalues,bins,patchs=plt.hist(data,bins=8)
                print(yvalues)
                hist_data,edges=np.histogram(data,bins=60,range=(-50,50))
                matrix_data.append(hist_data)
                xvalues = (bins[:-1] + bins[1:]) / 2
                guesses=(80,round(np.mean(data),5),round(np.std(data),5))
                print(guesses)
                #results,cc=curve_fit(gaussian,xvalues[yvalues>0],yvalues[yvalues>0],p0=guesses,maxfev=5000)
                #plt.plot(xvalues,gaussian(xvalues,*results),"r",label=f"Mean={round(results[1],4)}\nstd={round(results[2],4)}")
                #print(results)
                plt.title(f"Bin {i} For {self.distribution} with {iteration} Iterations")
                #plt.legend()
                plt.show()
                #if(i > 1 and i <6):
                #    plt.savefig(f"{self.distribution}_Bin_{i}_hist.pdf")
                plt.close()
                i+=1
            except Exception as e:
                print(e)
                plt.show()
                continue
        print(matrix_data)
        matrix_data=[x for _,x in sorted(zip(MJJ_BINNING,matrix_data))]
        fig, ax = plt.subplots(1,1)
        img = ax.imshow(np.array(matrix_data).transpose(),extent=[0,12,0.5,-0.5],aspect=10)
        ax.set_xticklabels(MJJ_BINNING)
        ax.set_ylabel("Truth-Unfolded")
        ax.set_xlabel("Bin Content")
        ax.set_title(f"{self.distribution} for Iteration {iteration}")
        
        fig.colorbar(img, ax=ax)
        #plt.savefig(f'{self.distribution}_Iteration_{iteration}.pdf')
        plt.show()   
        
            
            

    def total_unfolding_error_vs_iteration(self):
        integral=[0]*self.iterations
        for j in range(self.num_of_columns-2):
            for i in range(self.iterations):
                print(j,i)
                integral[i]+=self.processed_data[i][j+1][0]
        x=[x+1 for x in range(len(integral))][:10]
        print(integral)
        plt.plot(x,integral[:10]/np.sum(self.binning),label=f"Average Unfolding Error")
        plt.title(f"Integrated Unfolding Difference vs bayesian iterations")
        plt.xlabel("Bayesian Iterations")
        plt.ylabel("Average Difference Unfolded and Weighted Truth")

    def get_total_error(self,iteration):
        total_error=0
        for j in range(self.num_of_columns):
            total_error+=self.processed_data[j][iteration-1][0]
        return total_error
    
    def unfolding_error_vs_events(self):
        per_bin_error=np.empty((self.num_of_columns,self.iterations))
        for j in range(self.num_of_columns-2):
            for i in range(self.iterations):
                per_bin_error[j,i]=self.processed_data[i][j][0]
                
        for i in range(self.iterations-3):
            plt.plot(self.binning[1:],per_bin_error[:,i][1:19],"-*",label=f"{i+1} iterations")

        plt.legend()
        plt.title(f"average unfolding error vs. number of bin events for {self.distribution}")
        plt.show()

    def error_per_bin_vs_iteration(self):
        x=[x+1 for x in range(self.iterations)]
                
        for j in range(self.num_of_columns):
            bin_error=[0]*self.iterations
            print(len(self.processed_data))
            print(len(self.processed_data[1]))
            print(len(self.processed_data[1][1]))
            
            for i in range(self.iterations):
                try:
                    bin_error[i]=self.processed_data[i][j][0]
                except:
                    break
            plt.title(f"Error vs. Iterations for bin {j} for {self.distribution}")
            plt.plot(x,bin_error,label=f"Bin {j}")
            plt.xlabel("iteration")
            plt.ylabel("bin error")
            plt.legend()
            if(j%5==0):
                plt.show()
        plt.show()
            
    def get_error_per_bin(self,iteration):
        error_data=np.empty(self.num_of_columns)
        print(self.binning)
        for j in range(self.num_of_columns-2):
            print(self.binning[j])
            error_data[j]=self.processed_data[iteration-1][j][0]/self.binning[j]
        return error_data

    def plot_error_vs_bin(self,iteration):
        bin_error_list=self.get_error_per_bin(iteration)
        plt.plot([x for x in range(len(bin_error_list[1:-2]))],bin_error_list[1:-2],"g*-",label=f"Fluctuation Error")
        #plt.plot([x for x in range(len(DETALL_UNCERT))],[sig/events for sig,events in zip(DETALL_UNCERT,DETALL_BINNING)],"b*-",label="Stat. Uncertanty")
        plt.legend()

    def error_vs_off_diagonal(self,iteration):
        bin_error_list=self.get_error_per_bin(iteration)
        off_diagonal=np.sum(self.response_matrix,axis=1)-self.response_matrix.diagonal()
        plt.plot(np.absolute(bin_error_list[1:-1])[np.absolute(bin_error_list[1:-1])<1],off_diagonal[np.absolute(bin_error_list[1:-1])<1],'g.')
        plt.title(f"off diagonal_elements vs. bin error {self.distribution} with {iteration} iterations")
        plt.xlabel("bin error")
        plt.ylabel("off diagonal  elements")
        plt.show()

    def error_std_normalized(self):
        bin_data=[]
        for j in range(self.num_of_columns-2):
            iteration_data=[]
            for i in range(self.iterations-1):
                try:
                    iteration_data.append(self.processed_data[i+1][j+1][0]/self.processed_data[i+1][j+1][1])
                except:
                    pass
            bin_data.append(iteration_data)

        i=1
        j=1
        for bin_n in bin_data:
            plt.subplot(2,3,j%7)
            x=[i+1 for i in range(len(bin_n))]
            plt.plot(x,bin_n,"b.-")
            plt.title(f"{self.distribution} bin {i+1}  mean/std vs. Iteration")
            #plt.ylabel("Abs(mean)/standard deviation")
            if(j % 6 ==0):
               plt.show()
               plt.cla()
               j=1
            else:
                j+=1
            i+=1


if __name__=="__main__":
    matrices=load_matrices("response_matrix.txt")
    unfolding_error=load_unfolding_uncertainty()
    one_sigma_mjj=BinData("mjj_sig_1.0residual_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=19)
    one_sigma_mjj.error_per_bin_vs_iteration()
    #total
    one_sigma_mjj.total_unfolding_error_vs_iteration()
    plt.show()

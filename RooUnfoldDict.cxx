// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIhomedIguydICprojectsdIBuildUnfolddIRooUnfolddIRooUnfoldDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "include/RooUnfold.h"
#include "include/RooUnfoldBayes.h"
#include "include/RooUnfoldBinByBin.h"
#include "include/RooUnfoldErrors.h"
#include "include/RooUnfoldInvert.h"
#include "include/RooUnfoldParms.h"
#include "include/RooUnfoldResponse.h"
#include "include/RooUnfoldSvd.h"
#include "include/RooUnfoldTUnfold.h"
#include "include/RooUnfoldUtils.h"
#include "include/TSVDUnfold_local.h"
#include "include/TauSVDUnfold.h"

// Header files passed via #pragma extra_include

namespace RooUnfoldUtils {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *RooUnfoldUtils_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("RooUnfoldUtils", 0 /*version*/, "include/RooUnfoldUtils.h", 12,
                     ::ROOT::DefineBehavior((void*)0,(void*)0),
                     &RooUnfoldUtils_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *RooUnfoldUtils_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static void *new_RooUnfold(void *p = 0);
   static void *newArray_RooUnfold(Long_t size, void *p);
   static void delete_RooUnfold(void *p);
   static void deleteArray_RooUnfold(void *p);
   static void destruct_RooUnfold(void *p);
   static void streamer_RooUnfold(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfold*)
   {
      ::RooUnfold *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfold >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfold", ::RooUnfold::Class_Version(), "include/RooUnfold.h", 23,
                  typeid(::RooUnfold), DefineBehavior(ptr, ptr),
                  &::RooUnfold::Dictionary, isa_proxy, 17,
                  sizeof(::RooUnfold) );
      instance.SetNew(&new_RooUnfold);
      instance.SetNewArray(&newArray_RooUnfold);
      instance.SetDelete(&delete_RooUnfold);
      instance.SetDeleteArray(&deleteArray_RooUnfold);
      instance.SetDestructor(&destruct_RooUnfold);
      instance.SetStreamerFunc(&streamer_RooUnfold);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfold*)
   {
      return GenerateInitInstanceLocal((::RooUnfold*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfold*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldBayes(void *p = 0);
   static void *newArray_RooUnfoldBayes(Long_t size, void *p);
   static void delete_RooUnfoldBayes(void *p);
   static void deleteArray_RooUnfoldBayes(void *p);
   static void destruct_RooUnfoldBayes(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldBayes*)
   {
      ::RooUnfoldBayes *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldBayes >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldBayes", ::RooUnfoldBayes::Class_Version(), "include/RooUnfoldBayes.h", 24,
                  typeid(::RooUnfoldBayes), DefineBehavior(ptr, ptr),
                  &::RooUnfoldBayes::Dictionary, isa_proxy, 4,
                  sizeof(::RooUnfoldBayes) );
      instance.SetNew(&new_RooUnfoldBayes);
      instance.SetNewArray(&newArray_RooUnfoldBayes);
      instance.SetDelete(&delete_RooUnfoldBayes);
      instance.SetDeleteArray(&deleteArray_RooUnfoldBayes);
      instance.SetDestructor(&destruct_RooUnfoldBayes);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldBayes*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldBayes*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldBayes*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldSvd(void *p = 0);
   static void *newArray_RooUnfoldSvd(Long_t size, void *p);
   static void delete_RooUnfoldSvd(void *p);
   static void deleteArray_RooUnfoldSvd(void *p);
   static void destruct_RooUnfoldSvd(void *p);
   static void streamer_RooUnfoldSvd(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldSvd*)
   {
      ::RooUnfoldSvd *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldSvd >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldSvd", ::RooUnfoldSvd::Class_Version(), "include/RooUnfoldSvd.h", 23,
                  typeid(::RooUnfoldSvd), DefineBehavior(ptr, ptr),
                  &::RooUnfoldSvd::Dictionary, isa_proxy, 17,
                  sizeof(::RooUnfoldSvd) );
      instance.SetNew(&new_RooUnfoldSvd);
      instance.SetNewArray(&newArray_RooUnfoldSvd);
      instance.SetDelete(&delete_RooUnfoldSvd);
      instance.SetDeleteArray(&deleteArray_RooUnfoldSvd);
      instance.SetDestructor(&destruct_RooUnfoldSvd);
      instance.SetStreamerFunc(&streamer_RooUnfoldSvd);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldSvd*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldSvd*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldSvd*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldBinByBin(void *p = 0);
   static void *newArray_RooUnfoldBinByBin(Long_t size, void *p);
   static void delete_RooUnfoldBinByBin(void *p);
   static void deleteArray_RooUnfoldBinByBin(void *p);
   static void destruct_RooUnfoldBinByBin(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldBinByBin*)
   {
      ::RooUnfoldBinByBin *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldBinByBin >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldBinByBin", ::RooUnfoldBinByBin::Class_Version(), "include/RooUnfoldBinByBin.h", 23,
                  typeid(::RooUnfoldBinByBin), DefineBehavior(ptr, ptr),
                  &::RooUnfoldBinByBin::Dictionary, isa_proxy, 4,
                  sizeof(::RooUnfoldBinByBin) );
      instance.SetNew(&new_RooUnfoldBinByBin);
      instance.SetNewArray(&newArray_RooUnfoldBinByBin);
      instance.SetDelete(&delete_RooUnfoldBinByBin);
      instance.SetDeleteArray(&deleteArray_RooUnfoldBinByBin);
      instance.SetDestructor(&destruct_RooUnfoldBinByBin);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldBinByBin*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldBinByBin*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldBinByBin*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldResponse(void *p = 0);
   static void *newArray_RooUnfoldResponse(Long_t size, void *p);
   static void delete_RooUnfoldResponse(void *p);
   static void deleteArray_RooUnfoldResponse(void *p);
   static void destruct_RooUnfoldResponse(void *p);
   static void streamer_RooUnfoldResponse(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldResponse*)
   {
      ::RooUnfoldResponse *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldResponse >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldResponse", ::RooUnfoldResponse::Class_Version(), "include/RooUnfoldResponse.h", 27,
                  typeid(::RooUnfoldResponse), DefineBehavior(ptr, ptr),
                  &::RooUnfoldResponse::Dictionary, isa_proxy, 17,
                  sizeof(::RooUnfoldResponse) );
      instance.SetNew(&new_RooUnfoldResponse);
      instance.SetNewArray(&newArray_RooUnfoldResponse);
      instance.SetDelete(&delete_RooUnfoldResponse);
      instance.SetDeleteArray(&deleteArray_RooUnfoldResponse);
      instance.SetDestructor(&destruct_RooUnfoldResponse);
      instance.SetStreamerFunc(&streamer_RooUnfoldResponse);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldResponse*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldResponse*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldResponse*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_RooUnfoldErrors(void *p);
   static void deleteArray_RooUnfoldErrors(void *p);
   static void destruct_RooUnfoldErrors(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldErrors*)
   {
      ::RooUnfoldErrors *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldErrors >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldErrors", ::RooUnfoldErrors::Class_Version(), "include/RooUnfoldErrors.h", 21,
                  typeid(::RooUnfoldErrors), DefineBehavior(ptr, ptr),
                  &::RooUnfoldErrors::Dictionary, isa_proxy, 4,
                  sizeof(::RooUnfoldErrors) );
      instance.SetDelete(&delete_RooUnfoldErrors);
      instance.SetDeleteArray(&deleteArray_RooUnfoldErrors);
      instance.SetDestructor(&destruct_RooUnfoldErrors);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldErrors*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldErrors*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldErrors*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldParms(void *p = 0);
   static void *newArray_RooUnfoldParms(Long_t size, void *p);
   static void delete_RooUnfoldParms(void *p);
   static void deleteArray_RooUnfoldParms(void *p);
   static void destruct_RooUnfoldParms(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldParms*)
   {
      ::RooUnfoldParms *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldParms >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldParms", ::RooUnfoldParms::Class_Version(), "include/RooUnfoldParms.h", 22,
                  typeid(::RooUnfoldParms), DefineBehavior(ptr, ptr),
                  &::RooUnfoldParms::Dictionary, isa_proxy, 4,
                  sizeof(::RooUnfoldParms) );
      instance.SetNew(&new_RooUnfoldParms);
      instance.SetNewArray(&newArray_RooUnfoldParms);
      instance.SetDelete(&delete_RooUnfoldParms);
      instance.SetDeleteArray(&deleteArray_RooUnfoldParms);
      instance.SetDestructor(&destruct_RooUnfoldParms);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldParms*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldParms*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldParms*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldInvert(void *p = 0);
   static void *newArray_RooUnfoldInvert(Long_t size, void *p);
   static void delete_RooUnfoldInvert(void *p);
   static void deleteArray_RooUnfoldInvert(void *p);
   static void destruct_RooUnfoldInvert(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldInvert*)
   {
      ::RooUnfoldInvert *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldInvert >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldInvert", ::RooUnfoldInvert::Class_Version(), "include/RooUnfoldInvert.h", 25,
                  typeid(::RooUnfoldInvert), DefineBehavior(ptr, ptr),
                  &::RooUnfoldInvert::Dictionary, isa_proxy, 4,
                  sizeof(::RooUnfoldInvert) );
      instance.SetNew(&new_RooUnfoldInvert);
      instance.SetNewArray(&newArray_RooUnfoldInvert);
      instance.SetDelete(&delete_RooUnfoldInvert);
      instance.SetDeleteArray(&deleteArray_RooUnfoldInvert);
      instance.SetDestructor(&destruct_RooUnfoldInvert);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldInvert*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldInvert*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldInvert*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooUnfoldTUnfold(void *p = 0);
   static void *newArray_RooUnfoldTUnfold(Long_t size, void *p);
   static void delete_RooUnfoldTUnfold(void *p);
   static void deleteArray_RooUnfoldTUnfold(void *p);
   static void destruct_RooUnfoldTUnfold(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooUnfoldTUnfold*)
   {
      ::RooUnfoldTUnfold *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooUnfoldTUnfold >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooUnfoldTUnfold", ::RooUnfoldTUnfold::Class_Version(), "include/RooUnfoldTUnfold.h", 23,
                  typeid(::RooUnfoldTUnfold), DefineBehavior(ptr, ptr),
                  &::RooUnfoldTUnfold::Dictionary, isa_proxy, 4,
                  sizeof(::RooUnfoldTUnfold) );
      instance.SetNew(&new_RooUnfoldTUnfold);
      instance.SetNewArray(&newArray_RooUnfoldTUnfold);
      instance.SetDelete(&delete_RooUnfoldTUnfold);
      instance.SetDeleteArray(&deleteArray_RooUnfoldTUnfold);
      instance.SetDestructor(&destruct_RooUnfoldTUnfold);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooUnfoldTUnfold*)
   {
      return GenerateInitInstanceLocal((::RooUnfoldTUnfold*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RooUnfoldTUnfold*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_TauSVDUnfold(void *p);
   static void deleteArray_TauSVDUnfold(void *p);
   static void destruct_TauSVDUnfold(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TauSVDUnfold*)
   {
      ::TauSVDUnfold *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TauSVDUnfold >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TauSVDUnfold", ::TauSVDUnfold::Class_Version(), "include/TauSVDUnfold.h", 18,
                  typeid(::TauSVDUnfold), DefineBehavior(ptr, ptr),
                  &::TauSVDUnfold::Dictionary, isa_proxy, 4,
                  sizeof(::TauSVDUnfold) );
      instance.SetDelete(&delete_TauSVDUnfold);
      instance.SetDeleteArray(&deleteArray_TauSVDUnfold);
      instance.SetDestructor(&destruct_TauSVDUnfold);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TauSVDUnfold*)
   {
      return GenerateInitInstanceLocal((::TauSVDUnfold*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TauSVDUnfold*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_TSVDUnfold_local(void *p);
   static void deleteArray_TSVDUnfold_local(void *p);
   static void destruct_TSVDUnfold_local(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TSVDUnfold_local*)
   {
      ::TSVDUnfold_local *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TSVDUnfold_local >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TSVDUnfold_local", ::TSVDUnfold_local::Class_Version(), "include/TSVDUnfold_local.h", 54,
                  typeid(::TSVDUnfold_local), DefineBehavior(ptr, ptr),
                  &::TSVDUnfold_local::Dictionary, isa_proxy, 4,
                  sizeof(::TSVDUnfold_local) );
      instance.SetDelete(&delete_TSVDUnfold_local);
      instance.SetDeleteArray(&deleteArray_TSVDUnfold_local);
      instance.SetDestructor(&destruct_TSVDUnfold_local);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TSVDUnfold_local*)
   {
      return GenerateInitInstanceLocal((::TSVDUnfold_local*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TSVDUnfold_local*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooUnfold::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfold::Class_Name()
{
   return "RooUnfold";
}

//______________________________________________________________________________
const char *RooUnfold::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfold*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfold::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfold*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfold::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfold*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfold::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfold*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldBayes::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldBayes::Class_Name()
{
   return "RooUnfoldBayes";
}

//______________________________________________________________________________
const char *RooUnfoldBayes::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBayes*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldBayes::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBayes*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldBayes::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBayes*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldBayes::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBayes*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldSvd::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldSvd::Class_Name()
{
   return "RooUnfoldSvd";
}

//______________________________________________________________________________
const char *RooUnfoldSvd::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldSvd*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldSvd::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldSvd*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldSvd::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldSvd*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldSvd::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldSvd*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldBinByBin::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldBinByBin::Class_Name()
{
   return "RooUnfoldBinByBin";
}

//______________________________________________________________________________
const char *RooUnfoldBinByBin::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBinByBin*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldBinByBin::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBinByBin*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldBinByBin::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBinByBin*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldBinByBin::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldBinByBin*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldResponse::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldResponse::Class_Name()
{
   return "RooUnfoldResponse";
}

//______________________________________________________________________________
const char *RooUnfoldResponse::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldResponse*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldResponse::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldResponse*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldResponse::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldResponse*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldResponse::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldResponse*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldErrors::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldErrors::Class_Name()
{
   return "RooUnfoldErrors";
}

//______________________________________________________________________________
const char *RooUnfoldErrors::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldErrors*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldErrors::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldErrors*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldErrors::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldErrors*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldErrors::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldErrors*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldParms::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldParms::Class_Name()
{
   return "RooUnfoldParms";
}

//______________________________________________________________________________
const char *RooUnfoldParms::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldParms*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldParms::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldParms*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldParms::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldParms*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldParms::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldParms*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldInvert::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldInvert::Class_Name()
{
   return "RooUnfoldInvert";
}

//______________________________________________________________________________
const char *RooUnfoldInvert::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldInvert*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldInvert::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldInvert*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldInvert::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldInvert*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldInvert::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldInvert*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooUnfoldTUnfold::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooUnfoldTUnfold::Class_Name()
{
   return "RooUnfoldTUnfold";
}

//______________________________________________________________________________
const char *RooUnfoldTUnfold::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldTUnfold*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooUnfoldTUnfold::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldTUnfold*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooUnfoldTUnfold::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldTUnfold*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooUnfoldTUnfold::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooUnfoldTUnfold*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TauSVDUnfold::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TauSVDUnfold::Class_Name()
{
   return "TauSVDUnfold";
}

//______________________________________________________________________________
const char *TauSVDUnfold::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TauSVDUnfold*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TauSVDUnfold::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TauSVDUnfold*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TauSVDUnfold::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TauSVDUnfold*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TauSVDUnfold::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TauSVDUnfold*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TSVDUnfold_local::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TSVDUnfold_local::Class_Name()
{
   return "TSVDUnfold_local";
}

//______________________________________________________________________________
const char *TSVDUnfold_local::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TSVDUnfold_local*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TSVDUnfold_local::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TSVDUnfold_local*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TSVDUnfold_local::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TSVDUnfold_local*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TSVDUnfold_local::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TSVDUnfold_local*)0x0)->GetClass(); }
   return fgIsA;
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfold(void *p) {
      return  p ? new(p) ::RooUnfold : new ::RooUnfold;
   }
   static void *newArray_RooUnfold(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfold[nElements] : new ::RooUnfold[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfold(void *p) {
      delete ((::RooUnfold*)p);
   }
   static void deleteArray_RooUnfold(void *p) {
      delete [] ((::RooUnfold*)p);
   }
   static void destruct_RooUnfold(void *p) {
      typedef ::RooUnfold current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooUnfold(TBuffer &buf, void *obj) {
      ((::RooUnfold*)obj)->::RooUnfold::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooUnfold

//______________________________________________________________________________
void RooUnfoldBayes::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooUnfoldBayes.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooUnfoldBayes::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooUnfoldBayes::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldBayes(void *p) {
      return  p ? new(p) ::RooUnfoldBayes : new ::RooUnfoldBayes;
   }
   static void *newArray_RooUnfoldBayes(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldBayes[nElements] : new ::RooUnfoldBayes[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldBayes(void *p) {
      delete ((::RooUnfoldBayes*)p);
   }
   static void deleteArray_RooUnfoldBayes(void *p) {
      delete [] ((::RooUnfoldBayes*)p);
   }
   static void destruct_RooUnfoldBayes(void *p) {
      typedef ::RooUnfoldBayes current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooUnfoldBayes

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldSvd(void *p) {
      return  p ? new(p) ::RooUnfoldSvd : new ::RooUnfoldSvd;
   }
   static void *newArray_RooUnfoldSvd(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldSvd[nElements] : new ::RooUnfoldSvd[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldSvd(void *p) {
      delete ((::RooUnfoldSvd*)p);
   }
   static void deleteArray_RooUnfoldSvd(void *p) {
      delete [] ((::RooUnfoldSvd*)p);
   }
   static void destruct_RooUnfoldSvd(void *p) {
      typedef ::RooUnfoldSvd current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooUnfoldSvd(TBuffer &buf, void *obj) {
      ((::RooUnfoldSvd*)obj)->::RooUnfoldSvd::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooUnfoldSvd

//______________________________________________________________________________
void RooUnfoldBinByBin::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooUnfoldBinByBin.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooUnfoldBinByBin::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooUnfoldBinByBin::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldBinByBin(void *p) {
      return  p ? new(p) ::RooUnfoldBinByBin : new ::RooUnfoldBinByBin;
   }
   static void *newArray_RooUnfoldBinByBin(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldBinByBin[nElements] : new ::RooUnfoldBinByBin[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldBinByBin(void *p) {
      delete ((::RooUnfoldBinByBin*)p);
   }
   static void deleteArray_RooUnfoldBinByBin(void *p) {
      delete [] ((::RooUnfoldBinByBin*)p);
   }
   static void destruct_RooUnfoldBinByBin(void *p) {
      typedef ::RooUnfoldBinByBin current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooUnfoldBinByBin

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldResponse(void *p) {
      return  p ? new(p) ::RooUnfoldResponse : new ::RooUnfoldResponse;
   }
   static void *newArray_RooUnfoldResponse(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldResponse[nElements] : new ::RooUnfoldResponse[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldResponse(void *p) {
      delete ((::RooUnfoldResponse*)p);
   }
   static void deleteArray_RooUnfoldResponse(void *p) {
      delete [] ((::RooUnfoldResponse*)p);
   }
   static void destruct_RooUnfoldResponse(void *p) {
      typedef ::RooUnfoldResponse current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RooUnfoldResponse(TBuffer &buf, void *obj) {
      ((::RooUnfoldResponse*)obj)->::RooUnfoldResponse::Streamer(buf);
   }
} // end of namespace ROOT for class ::RooUnfoldResponse

//______________________________________________________________________________
void RooUnfoldErrors::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooUnfoldErrors.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooUnfoldErrors::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooUnfoldErrors::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RooUnfoldErrors(void *p) {
      delete ((::RooUnfoldErrors*)p);
   }
   static void deleteArray_RooUnfoldErrors(void *p) {
      delete [] ((::RooUnfoldErrors*)p);
   }
   static void destruct_RooUnfoldErrors(void *p) {
      typedef ::RooUnfoldErrors current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooUnfoldErrors

//______________________________________________________________________________
void RooUnfoldParms::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooUnfoldParms.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooUnfoldParms::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooUnfoldParms::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldParms(void *p) {
      return  p ? new(p) ::RooUnfoldParms : new ::RooUnfoldParms;
   }
   static void *newArray_RooUnfoldParms(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldParms[nElements] : new ::RooUnfoldParms[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldParms(void *p) {
      delete ((::RooUnfoldParms*)p);
   }
   static void deleteArray_RooUnfoldParms(void *p) {
      delete [] ((::RooUnfoldParms*)p);
   }
   static void destruct_RooUnfoldParms(void *p) {
      typedef ::RooUnfoldParms current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooUnfoldParms

//______________________________________________________________________________
void RooUnfoldInvert::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooUnfoldInvert.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooUnfoldInvert::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooUnfoldInvert::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldInvert(void *p) {
      return  p ? new(p) ::RooUnfoldInvert : new ::RooUnfoldInvert;
   }
   static void *newArray_RooUnfoldInvert(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldInvert[nElements] : new ::RooUnfoldInvert[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldInvert(void *p) {
      delete ((::RooUnfoldInvert*)p);
   }
   static void deleteArray_RooUnfoldInvert(void *p) {
      delete [] ((::RooUnfoldInvert*)p);
   }
   static void destruct_RooUnfoldInvert(void *p) {
      typedef ::RooUnfoldInvert current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooUnfoldInvert

//______________________________________________________________________________
void RooUnfoldTUnfold::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooUnfoldTUnfold.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooUnfoldTUnfold::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooUnfoldTUnfold::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooUnfoldTUnfold(void *p) {
      return  p ? new(p) ::RooUnfoldTUnfold : new ::RooUnfoldTUnfold;
   }
   static void *newArray_RooUnfoldTUnfold(Long_t nElements, void *p) {
      return p ? new(p) ::RooUnfoldTUnfold[nElements] : new ::RooUnfoldTUnfold[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooUnfoldTUnfold(void *p) {
      delete ((::RooUnfoldTUnfold*)p);
   }
   static void deleteArray_RooUnfoldTUnfold(void *p) {
      delete [] ((::RooUnfoldTUnfold*)p);
   }
   static void destruct_RooUnfoldTUnfold(void *p) {
      typedef ::RooUnfoldTUnfold current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooUnfoldTUnfold

//______________________________________________________________________________
void TauSVDUnfold::Streamer(TBuffer &R__b)
{
   // Stream an object of class TauSVDUnfold.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TauSVDUnfold::Class(),this);
   } else {
      R__b.WriteClassBuffer(TauSVDUnfold::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TauSVDUnfold(void *p) {
      delete ((::TauSVDUnfold*)p);
   }
   static void deleteArray_TauSVDUnfold(void *p) {
      delete [] ((::TauSVDUnfold*)p);
   }
   static void destruct_TauSVDUnfold(void *p) {
      typedef ::TauSVDUnfold current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TauSVDUnfold

//______________________________________________________________________________
void TSVDUnfold_local::Streamer(TBuffer &R__b)
{
   // Stream an object of class TSVDUnfold_local.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TSVDUnfold_local::Class(),this);
   } else {
      R__b.WriteClassBuffer(TSVDUnfold_local::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TSVDUnfold_local(void *p) {
      delete ((::TSVDUnfold_local*)p);
   }
   static void deleteArray_TSVDUnfold_local(void *p) {
      delete [] ((::TSVDUnfold_local*)p);
   }
   static void destruct_TSVDUnfold_local(void *p) {
      typedef ::TSVDUnfold_local current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TSVDUnfold_local

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 214,
                  typeid(vector<int>), DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 214,
                  typeid(vector<double>), DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 4,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace {
  void TriggerDictionaryInitialization_RooUnfoldDict_Impl() {
    static const char* headers[] = {
"include/RooUnfold.h",
"include/RooUnfoldBayes.h",
"include/RooUnfoldBinByBin.h",
"include/RooUnfoldErrors.h",
"include/RooUnfoldInvert.h",
"include/RooUnfoldParms.h",
"include/RooUnfoldResponse.h",
"include/RooUnfoldSvd.h",
"include/RooUnfoldTUnfold.h",
"include/RooUnfoldUtils.h",
"include/TSVDUnfold_local.h",
"include/TauSVDUnfold.h",
0
    };
    static const char* includePaths[] = {
"/home/guy/Cprojects/cern/NewRootBuild/include",
"/home/guy/Cprojects/BuildUnfold/RooUnfold/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$string")))  allocator;
}
class __attribute__((annotate(R"ATTRDUMP(Unfolding base class: implementations in RooUnfoldBayes, RooUnfoldSvd, RooUnfoldBinByBin, RooUnfoldTUnfold, and RooUnfoldInvert)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfold.h")))  RooUnfold;
class __attribute__((annotate(R"ATTRDUMP(Bayesian Unfolding)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldBayes.h")))  RooUnfoldBayes;
class __attribute__((annotate(R"ATTRDUMP(SVD Unfolding (interface to TSVDUnfold))ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldSvd.h")))  RooUnfoldSvd;
class __attribute__((annotate(R"ATTRDUMP(Bin-by-bin unfolding)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldBinByBin.h")))  RooUnfoldBinByBin;
class __attribute__((annotate(R"ATTRDUMP(Respose Matrix)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfold.h")))  RooUnfoldResponse;
class __attribute__((annotate(R"ATTRDUMP(Show unfolding errors)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldErrors.h")))  RooUnfoldErrors;
class __attribute__((annotate(R"ATTRDUMP(Optimisation of unfolding regularisation parameter)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldParms.h")))  RooUnfoldParms;
class __attribute__((annotate(R"ATTRDUMP(Unregularised unfolding)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldInvert.h")))  RooUnfoldInvert;
class __attribute__((annotate(R"ATTRDUMP(Interface to TUnfold)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/RooUnfoldTUnfold.h")))  RooUnfoldTUnfold;
class __attribute__((annotate("$clingAutoload$include/TauSVDUnfold.h")))  TauSVDUnfold;
class __attribute__((annotate(R"ATTRDUMP(Data unfolding using Singular Value Decomposition (hep-ph/9509307))ATTRDUMP"))) __attribute__((annotate("$clingAutoload$include/TSVDUnfold_local.h")))  TSVDUnfold_local;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "include/RooUnfold.h"
#include "include/RooUnfoldBayes.h"
#include "include/RooUnfoldBinByBin.h"
#include "include/RooUnfoldErrors.h"
#include "include/RooUnfoldInvert.h"
#include "include/RooUnfoldParms.h"
#include "include/RooUnfoldResponse.h"
#include "include/RooUnfoldSvd.h"
#include "include/RooUnfoldTUnfold.h"
#include "include/RooUnfoldUtils.h"
#include "include/TSVDUnfold_local.h"
#include "include/TauSVDUnfold.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"RooUnfold", payloadCode, "@",
"RooUnfoldBayes", payloadCode, "@",
"RooUnfoldBinByBin", payloadCode, "@",
"RooUnfoldErrors", payloadCode, "@",
"RooUnfoldInvert", payloadCode, "@",
"RooUnfoldParms", payloadCode, "@",
"RooUnfoldResponse", payloadCode, "@",
"RooUnfoldSvd", payloadCode, "@",
"RooUnfoldTUnfold", payloadCode, "@",
"TSVDUnfold_local", payloadCode, "@",
"TauSVDUnfold", payloadCode, "@",
"vector<Double_t>", payloadCode, "@",
"vector<Int_t>", payloadCode, "@",
"vector<double>", payloadCode, "@",
"vector<int>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RooUnfoldDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RooUnfoldDict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RooUnfoldDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RooUnfoldDict() {
  TriggerDictionaryInitialization_RooUnfoldDict_Impl();
}

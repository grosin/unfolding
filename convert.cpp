#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>


int convert(){
  TFile *from_file=TFile::Open("test.root");
  TH1F * jetPt=(TH1F*)from_file->Get("JetPt");
  
  TH1D MyJetPt;
  jetPt->Copy(MyJetPt);
  delete jetPt;
  TFile *to_file=new TFile("my_test.root","RECREATE");
  MyJetPt.Write();
  return 0;
}

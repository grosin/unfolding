#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TProfile.h>

void create_migration(){
  TFile * hists=TFile::Open("test.root");
  TH1F * xHist=(TH1F*)hists->Get("JetPt");
  TH1F * yHist=(TH1F*)hists->Get("JetPt_truth");

  TH2F * migration=(TH2F*)hists->Get("JetPt_reco_truth");
  //migration->Draw();
  TProfile * xProf=migration->ProfileX();
  TProfile *yProf=migration->ProfileY();
  xProf->Draw();
}

#include <TFile.h>
#include <TH1F.h>
#include <string>
#include <vector>
#include <sstream>

std::vector<TH1F *> get_histograms(std::string file){
  TFile *hist_file = TFile::Open(file.c_str());
  TDirectoryFile * nominal_file=(TDirectoryFile*)hist_file->Get("Nominal");

  std::cout<<"opened file\n";
  std::vector<TH1F*> hists;
  TH1F  * reco_hist=(TH1F*)nominal_file->Get("Nominal_MCReco");
  TH1F  * truth_hist=(TH1F*)nominal_file->Get("Nominal_MCTruth");
  TH1F  * unfolded_hist=(TH1F*)nominal_file->Get("Nominal_Unfolded");
  std::cout<<reco_hist->GetName()<<"\n";
  std::cout<<truth_hist->GetName()<<"\n";
  std::cout<<unfolded_hist->GetName()<<"\n";

  hists.push_back(reco_hist);
  hists.push_back(truth_hist);
  hists.push_back(unfolded_hist);
  return hists;
}
TH1F * subtract_hist(TH1F * truth, TH1F *unfold){
  TH1F * results=(TH1F*)unfold->Clone();
  int nbins=results->GetSize();
  double unfld_content;
  double truth_content;
  for(int i=0;i<nbins;i++){
    unfld_content=unfold->GetBinContent(i);
    truth_content=truth->GetBinContent(i);
    results->SetBinContent(i,truth_content-unfld_content);
  }
  return results;
}
void overlay_hists(){
  std::string home="vip_root_files/";
  std::ifstream file_list;
  std::string line;
  file_list.open("root_files.txt");
  TCanvas c1("cDraw", "cDraw",900,900);
  c1.cd();
  int i=0;
  std::vector<TH1F*> diffs;
  
  while(getline(file_list, line)){
    // std::cout<<line<<"\n";
    std::string file=home+line;
    std:: vector<TH1F*>hists=get_histograms(file);
    if(i==0){
      subtract_hist(hists[1],hists[2])->Draw("HIST");
            
      diffs.push_back(subtract_hist(hists[1],hists[2]));
      //hists[2]->Draw("HIST");
    }
    else{
      subtract_hist(hists[1],hists[2])->Draw("SAME");
      diffs.push_back(subtract_hist(hists[1],hists[2]));
      // hists[2]->Draw("SAME");
    }
    i++;
  }
  c1.Print("unfolded_hists.pdf");
  std::vector<TH1F*> gaussians(diffs[0]->GetSize());
  for(int i=0;i<diffs[0]->GetSize();i++){
    gaussians[i]=new TH1F((std::string("hist_of_bin")+std::to_string(i)).c_str(),(std::string("hist_of_bin")+std::to_string(i)).c_str(),9,600,3000);

  }
  for(auto &hist: diffs){
    for(int i=0;i<hist->GetSize();i++){
      gaussians[i]->Fill(hist->GetBinContent(i));
      if(i==13)
        std::cout<<i<<" "<<hist->GetBinContent(i)<<"\n";
    }

  }
  TCanvas c2("c2Draw", "c2Draw",900,900);
  c2.cd();
  gaussians[13]->Draw("HIST");
  c2.Print("gaussian_bins_13.pdf");

}
